defmodule MetexWeatherOtp.Worker do
  def fetch_location_temperature(location) do
    result =
      fetch_wttr_url(location)
      |> HTTPoison.get()
      |> parse_response

    case result do
      {:ok, temp} -> "#{location}: #{temp}º C"
      :error -> "#{location} not found!"
    end
  end

  defp fetch_wttr_url(location) do
    location = URI.encode(location)

    "https://wttr.in/#{location}?format=j1"
  end

  defp parse_response({:ok, %HTTPoison.Response{body: body, status_code: 200}}) do
    body
    |> Poison.decode!()
    |> compute_temperature()
  end

  defp parse_response(_) do
    :error
  end

  defp compute_temperature(%{"current_condition" => [%{"temp_C" => temp}]}) do
    try do
      {:ok, temp}
    rescue
      _ -> :error
    end
  end
end
