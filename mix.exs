defmodule MetexWeatherOtp.MixProject do
  use Mix.Project

  def project do
    [
      app: :metex_weather_otp,
      version: "0.0.1",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      # Start before deps
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 2.0.0"},
      {:poison, "~> 5.0"}
    ]
  end
end
